CAF Server
==========

This project provides a complex ambiguity function (CAF) implementation and a
service that runs CAFs in a docker.  It's mostly an excuse for me to learn more
about docker and microservices architecture.


Installation
------------

Ensure you have docker and docker-compose installed, and then simply do:

```sh
$ docker-compose up
```

in the repo root directory.


TODO
----

- Implement server push using SSE.
  [Reference](https://stackoverflow.com/questions/12232304/how-to-implement-server-push-in-flask-framework)
- Make page UI actually nice.
