import pickle

from caf.caf import Caf


with open('caf_res.pickle', 'rb') as f:
    caf_obj = pickle.load(f)

caf_obj.plot()
