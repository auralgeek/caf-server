import numpy as np

from caf.caf import Caf


n_sam = 100000
fs = 1e6
fdoppler = 120.456789
delay = 13

t = np.arange(n_sam) / fs
s1 = np.random.randn(n_sam) + 1j * np.random.randn(n_sam)
s2 = np.roll(s1 * np.exp(2j * np.pi * fdoppler * t), delay)
s2[:delay] = 0
s2 = s2[:50000]

caf_obj = Caf(s1, s2, fs, 200, 5)
caf_obj.execute()

print("TOA: {} s".format(caf_obj.toa))
print("FOA: {} Hz".format(caf_obj.foa))
print("SNR: {} dB".format(caf_obj.snr))
print("GAIN: {} dB".format(caf_obj.gain))

caf_obj.plot(tradius=50)
