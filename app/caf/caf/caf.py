import numpy as np
from scipy.signal import correlate


def execute_caf(s1, s2, fs, fradius, fres):
    caf_obj = Caf(s1, s2, fs, fradius, fres)
    caf_obj.execute()

    return caf_obj.toa, caf_obj.foa, caf_obj.snr


class Caf():
    """
    Object to perform a complex ambiguity function (CAF) on input signals s1
    and s2.  Also provides peak finding functionality to report delay and
    doppler detected by the CAF, and a plotting function to view the surface
    computed.

    Arguments:
    s1 -- the reference signal of the CAF operation
    s2 -- the signal being compared to the reference signal
    fs -- the sample rate of the input signals, in Hz
    fradius -- the frequency radius of the CAF search area, in Hz
    fres -- the frequency resolution for the CAF search area, in Hz
    """

    def __init__(self, s1, s2, fs, fradius, fres):
        self.s1 = s1
        self.s2 = s2
        self.fs = fs
        self.fradius = fradius
        self.fres = fres

        self.freqs = np.arange(-fradius, fradius + fres, fres)
        self.xcorr_len = len(s1) + len(s2) - 1

        self.surface = np.zeros((len(self.freqs), self.xcorr_len), dtype=np.float64)
        self.zero_idx = None
        self.toa = None
        self.foa = None
        self.snr = None
        self.gain = 10 * np.log10(min(len(s1), len(s2)) / fs * fs)

    def execute(self):
        """
        Once the CAF object is initialized, this method performs the actual
        computation of the CAF surface.
        """
        t = np.arange(len(self.s2)) / self.fs

        for i, freq in enumerate(self.freqs):
            s2_freq_shifted = np.exp(-2j * np.pi * freq * t) * self.s2
            self.surface[i, :] = np.abs(correlate(self.s1, s2_freq_shifted, mode='full'))

        # TOA and FOA
        i = np.argmax(self.surface)
        delay = i % self.xcorr_len
        self.zero_idx = min(len(self.s1), len(self.s2)) - 1

        self.toa = (self.zero_idx - delay) / self.fs
        self.foa = self.freqs[i // self.xcorr_len]

        # SNR
        sig_pwr_db = 10 * np.log10(np.max(self.surface) ** 2)
        noise_pwr_db = 10 * np.log10(np.mean(self.surface[i // self.xcorr_len, :] ** 2))
        self.snr = sig_pwr_db - noise_pwr_db

    def plot(self, tradius=100):
        """
        Method to display the CAF surface with some window centered on the 0
        delay, 0 doppler point.

        Keyword arguments:
        tradius -- the radius of the time axis to be displayed, in μs.
        """
        if not self.toa:
            raise Exception("Run Caf.execute() prior to plotting!")

        import matplotlib.pyplot as plt

        tstart = self.zero_idx - tradius
        tend = self.zero_idx + tradius

        time_axis = np.arange(-tradius, tradius + 1) / self.fs * 1e6

        X, Y = np.meshgrid(time_axis, self.freqs)
        Z = np.fliplr(self.surface[:, tstart:tend + 1])

        fig, ax = plt.subplots()
        cs = ax.contourf(X, Y, Z)
        cbar = fig.colorbar(cs)
        plt.xlabel(r"Delay (μs)")
        plt.ylabel("Doppler (Hz)")
        plt.show()
