import json
import pickle
import numpy as np
import matplotlib.pyplot as plt

import requests


def test_caf():
    n_sam = 100000
    fs = 1e6
    fdoppler = 120.456789
    delay_samples = 13
    delay = delay_samples / fs

    t = np.arange(n_sam) / fs
    s1 = np.random.randn(n_sam) + 1j * np.random.randn(n_sam)
    s2 = np.roll(s1 * np.exp(2j * np.pi * fdoppler * t), delay_samples)
    s2[:delay_samples] = 0
    s2 = s2[:50000]

    s1s = np.zeros(len(s1) * 2)
    s1s[::2] = np.real(s1)
    s1s[1::2] = np.imag(s1)

    s2s = np.zeros(len(s2) * 2)
    s2s[::2] = np.real(s2)
    s2s[1::2] = np.imag(s2)

    fradius = 200
    fres = 5

    data = {'s1': s1s.tolist(), 's2': s2s.tolist(), 'fs': fs, 'fradius': fradius, 'fres': fres}
    r = requests.post('http://localhost:5000/caf', json=data, timeout=5.0)
    print(r)
    print(r.status_code)
    print(r.json())

test_caf()
