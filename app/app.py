import flask
from flask import Flask, request, jsonify, url_for
import redis
import rq
import numpy as np
import threading
import time

from caf import Caf, execute_caf


app = Flask(__name__)
redis_conn = redis.Redis(host='redis', port=6379)
q = rq.Queue(connection=redis_conn)
finished = q.finished_job_registry


# Thread to collect CAF results
def collect_results():
    while True:
        time.sleep(0.1)
        for job_id in finished.get_job_ids():
            app.logger.info("Got CAF result from {}".format(job_id))
            job = q.fetch_job(job_id)
            app.logger.info(job.result)
            finished.remove(job_id)


@app.route("/")
def hello():
    results = []
    for job_id in finished.get_job_ids():
        job = q.fetch_job(job_id)
        app.logger.info(job.result[0])
        results.append({'id': job_id, 'toa': job.result[0], 'foa': job.result[1], 'snr': job.result[2]})

    return flask.render_template('index.html', results=results)


@app.route("/caf", methods=['POST'])
def caf_task_start():
    data = request.get_json()
    s1 = np.array(data['s1'], dtype=np.float64)
    s2 = np.array(data['s2'], dtype=np.float64)
    s1 = np.array(s1[::2] + 1j * s1[1::2], dtype=np.complex128)
    s2 = np.array(s2[::2] + 1j * s2[1::2], dtype=np.complex128)
    job = q.enqueue(execute_caf, s1, s2, data['fs'], data['fradius'], data['fres'])

    return jsonify({'status': job.get_status()})


if __name__ == '__main__':
    cthread = threading.Thread(target=collect_results, args=())
    #cthread.start()

    app.run(host="0.0.0.0", debug=True, threaded=True)

    cthread.join()
